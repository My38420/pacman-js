console.log('Coucou !')
import './css/style.scss'

// General constants
import {foodWidth, foodHeight, foodColor, widthFloor, heightFloor, step, sizePacman, heightGhost, widthGhost, wallDistance, maxFood,foodAte, angleDirection, directions, ghostLimitStraightLine, colorsGhost, aniimMouth, wallsInfos, foodsPosition}
 from '../constants.js';

const animMouth = " @keyframes eat {\
  0% {\
    clip-path: polygon(100% 74%, 44% 48%, 100% 21%);\
  }\
  25% {\
    clip-path: polygon(100% 60%, 44% 48%, 100% 40%);\
  }\
  50% {\
    clip-path: polygon(100% 50%, 44% 48%, 100% 50%);\
  }\
  75% {\
    clip-path: polygon(100% 59%, 44% 48%, 100% 35%);\
  }\
  100% {\
    clip-path: polygon(100% 74%, 44% 48%, 100% 21%);\
  }\
}\
";
//classes 
// import {Character} from '../classcharacter.js';
import {PacMan} from '../classpacman.js';
import {Ghost} from '../classghost.js';
const root = document.getElementById('root');

let intervalGhostId = null;
var s = document.createElement( 'style' );

s.innerHTML =  animMouth;
root.appendChild(s);

// create elements
const screen = document.createElement('div');
screen.setAttribute('id', 'screen');
screen.setAttribute('class', 'screen')


const gameFloor = document.createElement('div');
gameFloor.setAttribute('id', 'gameFloor');
gameFloor.setAttribute('class', 'gameFloor');
gameFloor.style.width = `${ widthFloor}px`;
gameFloor.style.height = `${ heightFloor}px`;



// initializing game elements
const pacMan = new PacMan(350, 50, wallsInfos, foodsPosition);

// Create Ghost
function generateGhosts() {
  const ghost1 = new Ghost(350, 350, wallsInfos, pacMan);
  gameFloor.appendChild(ghost1.getGhost());
  intervalGhostId = setInterval(() => {
    gameFloor.appendChild(new Ghost(350, 350, wallsInfos, pacMan).getGhost());
  }, 10000);
}

// Create walls
const walls = wallsInfos.map((wall, i) => {
  const w = document.createElement('div');
  w.setAttribute('id', `wall-${i}`);
  // w.setAttribute('class', 'wall');
  w.style.width = `${wall.width}px`;
  w.style.height = `${wall.height}px`;
  w.style.border = '#3F51B5 7px double';
  w.style.boxSizing = 'border-box';
  w.style.borderRadius = '2px';
  w.style.backgroundColor = 'black';
  w.style.position = 'absolute';
  w.style.top = `${wall.top}px`;
  w.style.left = `${wall.left}px`;
  return w;
});

// Create Food


const foods = foodsPosition.map((p, i) => {
  const f = document.createElement('div');
  f.setAttribute('id', p.id);
  // f.setAttribute('class','food');
  f.style.width = `${foodWidth}px`;
  f.style.height = `${foodHeight}px`;
  f.style.backgroundColor = foodColor;
  f.style.position = "absolute";
  f.style.top = `${p.top}px`;
  f.style.left = `${p.left}px`;
  return f;
});

function GameOver() {
  const card = document.createElement('div');
  card.setAttribute('class','card');
  // card.style.width = "300px";
  // card.style.height = "300px";
  // card.style.border = "solid 3px #3F51B5";
  // card.style.display = "flex";
  // card.style.justifyContent = "center";
  // card.style.alignItems = "center";
  // card.style.backgroundColor = "black";
  // card.style.zIndex= 999;
  // card.style.position = "absolute";
  card.style.left = `${ widthFloor/2 - 150}px`;
  card.style.top = `${heightFloor/2 - 150}px`;
  const text = document.createElement('div');
  text.innerHTML = "Game Over";
  text.style.color = "red";
  text.style.fontWeight = "bold";
  text.style.fontSize = "40px";
  card.appendChild(text);
  gameFloor.appendChild(card);
}

function Victory() {
  const card = document.createElement('div');
  card.setAttribute('class','card');
  // card.style.width = "300px";
  // card.style.height = "300px";
  // card.style.border = "solid 3px #3F51B5";
  // card.style.display = "flex";
  // card.style.justifyContent = "center";
  // card.style.alignItems = "center";
  // card.style.backgroundColor = "black";
  // card.style.zIndex= 999;
  // card.style.position = "absolute";
  card.style.left = `${ widthFloor/2 - 150}px`;
  card.style.top = `${heightFloor/2 - 150}px`;
  const text = document.createElement('div');
  text.innerHTML = "You won!!";
  text.style.color = "red";
  text.style.fontWeight = "bold";
  text.style.fontSize = "40px";
  card.appendChild(text);
  gameFloor.appendChild(card);
}

// we inject the element on root
walls.forEach(wall => gameFloor.appendChild(wall));
foods.forEach(food => gameFloor.appendChild(food));
gameFloor.appendChild(pacMan.getPacMan());
screen.appendChild(gameFloor);
root.appendChild(screen);

// We create the ghosts
generateGhosts();